﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace w3.Check
{
    //solid : S,L,I
    public class CheckValue : ICheckMin, ICheckMax
    {
        public bool IsMax(int val1, int val2)
        {
            return val1 > val2;
        }

        public bool IsMin(int val1, int val2)
        {
            return val1 < val2;
        }
    }
}
