﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace w3.Check
{
    public interface ICheckMin
    {
        bool IsMin(int val1, int val2);
    }
}
