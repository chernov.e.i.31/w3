﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace w3.Messenger
{
    //solid: S,D
    public class SendConsole : IMessenger
    {
        public void Send(string message)
        {
            Console.WriteLine(message);
        }
    }
}
