﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace w3.Messenger
{
    public interface IMessenger
    {
        public void Send(string message);
    }
}
