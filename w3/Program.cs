﻿using w3.Check;
using w3.Generate;
using w3.Messenger;

Console.WriteLine("Укажите диапазон, чисел \n Нижнее значение");
var min = Int32.Parse( Console.ReadLine());
Console.WriteLine("Верхнее значение");
var max = Int32.Parse(Console.ReadLine());
Console.WriteLine("Введите количество попыток");
var countToTry = Int32.Parse(Console.ReadLine());

var valueKey = new GeneratorRandom() { minNumber = min, maxNumber = max }.Number();

Console.WriteLine("Угадайте число");
var check = new CheckValue();
IMessenger messenger = new SendConsole();

var tryCount = 0;
while (tryCount< countToTry)
{
    var val = Int32.Parse(Console.ReadLine());
    if (check.IsMin(val, valueKey))
        messenger.Send("Число меньше загаданного");
    else if (check.IsMax(val, valueKey))
        messenger.Send("Число больше загаданного");
    else
    {
        messenger.Send("Число угадано");
        break;
    }
    tryCount++;
}
