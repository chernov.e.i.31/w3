﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace w3.Generate
{
    //solid: S,O
    public class GeneratorRandom : Generator
    {
        public int minNumber { get; set; }
        public int maxNumber { get; set; }
        public override int Number()
        {
            return Random.Shared.Next(minNumber, maxNumber);
        }
    }
}
