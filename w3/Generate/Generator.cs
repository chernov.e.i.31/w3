﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace w3.Generate
{
    public abstract class Generator
    {
        public abstract int Number();
    }
}
